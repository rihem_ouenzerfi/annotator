const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const passport = require('passport');

require('dotenv').config();
const users = require('./routes/api/user');
const images = require('./routes/api/image');
var cors = require('cors');

var app = express();

app.use(cors());
app.use(bodyParser.json());

app.use(
	bodyParser.urlencoded({
		extended: false,
		limit: '50mb',
	})
);

let connectionString =
	'mongodb://' + process.env.DATABASE_HOST + ':' + process.env.DATABASE_PORT + '/' + process.env.DATABASE_NAME;
mongoose
	.connect(connectionString, { useNewUrlParser: true })
	.then(() => console.log('MongoDB Connected'))
	.catch(err => console.log(err));

// Passport middleware
app.use(passport.initialize());

// Passport Config
require('./config/passport')(passport);

// Use Routes
app.use('/api/users', users);
app.use('/api/images', images);
app.use((req, res, next) => {
	res.header('Access-Control-Allow-Origin', '*');
	res.header('Access-Control-Allow-Headers', 'Origin,X-Requested-With, Content-Type, Accept, Authorization');
	if (req.method === 'OPTIONS') {
		res.header('Access-Control-Allow-Methods', 'PUT,POST ,PATCH,DELETE, GET');
	}
});
const port = process.env.PORT || 4000;

app.listen(port, function() {
	console.log('Listening on port ' + port);
});
