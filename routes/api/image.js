let Image = require('../../models/image.model');
const express = require('express');
const router = express.Router();
const passport = require('passport');


// @route   GET api/image/
// @desc   get images
// @access  Private
router.get('/', passport.authenticate('jwt', { session: false }), (req, res) => {
	if (req.query.classification) {
		classification = req.query.classification;
		Image.find({ classification: classification })
			.exec()
			.then(images => {
				res.status(200).json(images);
			})
			.catch(err => {
				console.log(err);
				res.status(404).json({ message: 'No Images found under this classification' });
			});
	} else {
		classification = req.query.classification;
		Image.find()
			.exec()
			.then(images => {
				res.status(200).json(images);
			})
			.catch(err => {
				console.log(err);
				res.status(404).json({ message: 'No Images found' });
			});
	}
});
// @route   GET api/image/:id
// @desc   get image by id
// @access  Private
router.get('/:id', passport.authenticate('jwt', { session: false }), (req, res) => {
	if (req.query.id == '') {
		res.status(400);
		res.send({
			error: 'EmptyRequest',
		});
		return;
	} else {
		id = req.query.id;
		Image.findById(id)
			.exec()
			.then(image => {
				res.status(200).json(image);
			})
			.catch(err => {
				console.log(err);
				res.status(404).json({ message: 'No Image found' });
			});
	}
});
// @route   POST api/image/
// @desc   save image with defect
// @access  Private
router.post('/:id', passport.authenticate('jwt', { session: false }), (req, res) => {
	if (req.body == '' || req.query.id == '') {
		res.status(400);
		res.send({
			error: 'EmptyRequest',
		});
		return;
	} else {
		id = req.query.id;
		Image.findById(id)
			.exec()
			.then(image => {
				image.defects.push({
					id_operator: req.body.id_operator,
					annotations: req.body.annotations,
				});
				image.save().then(image => res.json(image));
			})
			.catch(err => {
				console.log(err);
				res.status(404).json({ message: 'No Image found' });
			});
	}
});
module.exports = router;
