const mongoose = require('mongoose');

const Schema = mongoose.Schema;
let point_schema = new Schema({ x: Number, y: Number });
let circle_schema = new Schema({ radius: Number, center: point_schema });
let polygone_schema = new Schema({ points: [point_schema] });
let defect_annotations = new Schema({
	annontation_type: String,
	circles: circle_schema,
	polygone: polygone_schema,
	image: Buffer,
	paintBrushes: String,
	label: String,
});
let Image = new Schema(
	{
		classification: {
			type: String,
		},
		defects: [
			{
				id_operator: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
				annotations: [defect_annotations],
			},
		],
		image_path: String,
		image_title: String,
		image_extension: String,
	},
	{
		collection: 'image',
	}
);

module.exports = mongoose.model('Image', Image);
