import React from 'react';
import './App.css';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import { blue, indigo } from '@material-ui/core/colors';
import { Provider } from 'react-redux';
import { Route, Switch, HashRouter } from 'react-router-dom';

import Login from './components/Login';
import store from './store/Store';
const theme = createMuiTheme({
	palette: {
		secondary: {
			main: blue[900],
		},
		primary: {
			main: indigo[700],
		},
	},
	typography: {
		fontFamily: ['"Lato"', 'sans-serif'].join(','),
	},
});

function App() {
	return (
		<Provider store={store}>
			<div>
				<MuiThemeProvider theme={theme}>
					<HashRouter>
							<Switch>
								<Route path="/login" render={() => <Login />} />
								{/* <Route path="*" render={() => <h1>Not Found</h1>} /> */}
							</Switch>
					</HashRouter>
				</MuiThemeProvider>
			</div>
		</Provider>
	);
}

export default App;
