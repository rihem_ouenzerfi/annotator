import {SIGN_IN_ERR } from '../actions/types';

const initialState = {
    authErr: false,
};

export default (state = initialState, action) => {
	switch (action.type) {
		case SIGN_IN_ERR:
			return {
				...state,
				authErr:  true
			};
		 
		default:
			return state;
	}
};
