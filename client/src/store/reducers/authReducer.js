import { SIGN_IN, LOG_OUT } from '../actions/types';

const initialState = {
	user_id:'',
	token: '',
	authenticated: false,
};

export default (state = initialState, action) => {
	switch (action.type) {
		case SIGN_IN:
			return {
				...state,
				token: action.payload.token,
				user_id: action.payload.user_id,
				authenticated: true,
			};
		case LOG_OUT:
			return {
				...state,
				user_id:'',
				token: '',
				authenticated: false,
			};
		default:
			return state;
	}
};
