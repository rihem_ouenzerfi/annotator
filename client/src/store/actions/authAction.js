import { SIGN_IN, SIGN_IN_ERR, LOG_OUT } from './types';
import { AUTH_URL } from '../config';
import axios from 'axios';
export const sign_in = (login, pwd) => dispatch => {
	axios
		.post(`${AUTH_URL}`, { username: login, password: pwd })
		.then(response => {
			localStorage.setItem('idToken', response.data.token);
			localStorage.setItem('Iduser', response.data.user_id);
			console.log(response.data)
			dispatch({
				type: SIGN_IN,
				payload: response.data,
			});
		})
		.catch(err => {
			console.log('authactionerr', err);

			dispatch({
				type: SIGN_IN_ERR,
				payload: [],
			});
		});
};
export const log_out = (login, pwd) => dispatch => {
	localStorage.removeItem('idToken');

	dispatch({
		type: LOG_OUT,
	});
};
